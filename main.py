import asyncio
import logging

from config import *
from bot import Bot

logging.basicConfig(format='%(asctime)s - %(levelname)s:%(module)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.DEBUG)

bot = Bot(bot_token)

print(bot.token)

loop = asyncio.get_event_loop()
loop.create_task(bot.startPolling())

loop.run_forever()