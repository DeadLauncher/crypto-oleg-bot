import logging
import telegram
import asyncio

class Handler():
    def __init__(self, condition, executor):
        self.condition = condition
        self.executor = executor

    def execute(self, arg):
        if self.condition(arg):
            return self.executor()


handlers_message = []
handlers_callback = []

def terminate_confirmation():
    text = 'Are you sure you want to terminate the process?'
    markup = '{"inline_keyboard": [[{"text": "Yas", "callback_data": "terminate_confirm"}, {"text": "Nope", "callback_data": "terminate_discard"}]]}'
    return telegram.Answer(text, markup)

def terminate_discard():
    text = 'Termination canceled'
    return telegram.Answer(text, None)

def terminate_delay():
    text = 'Process will be terminated in 5 seconds'
    loop = asyncio.get_event_loop()
    loop.create_task(terminate())
    return telegram.Answer(text, None)

async def terminate():
    await asyncio.sleep(5)
    logging.warning('Process is shutting down')
    exit()

handlers_message.append(Handler(lambda text: text == 'terminate', terminate_confirmation))
handlers_callback.append(Handler(lambda callback: callback == 'terminate_confirm', terminate_delay))
handlers_callback.append(Handler(lambda callback: callback == 'terminate_discard', terminate_discard))
