import aiohttp
import logging

from handlers import handlers_message, handlers_callback
from telegram import Answer
from config import user_id

class Bot():
    def __init__(self, token):
        self.token = token 
        self.session = aiohttp.ClientSession()
        self.api_url = 'https://api.telegram.org/bot{}/'.format(token)
        self.stop = 0
        self.ts = 0

    async def get(self, method, params = None):
        async with self.session.get(self.api_url + method, params = params) as response:
            response = await response.json()
            try:
                assert response['ok'] == True
            except:
                logging.error('Response is not OK. Response: {}'.format(response))
                return []
            return response['result']

    async def getUpdates(self):
        update = await self.get('getUpdates', {'timeout': 30, 'offset': self.ts})
        if len(update) == 0:
            return None
        else:
            update = update[-1] 
            self.ts = update['update_id'] + 1
            return update

    async def startPolling(self):
        logging.debug("Polling has started. Token: {}.".format(self.token))
        while self.stop == 0:
            update = await self.getUpdates()
            if update:
                print(update)
                
                if 'callback_query' in update:
                    handlers = handlers_callback
                    param = update['callback_query']['data']
                    answer = Answer(update['callback_query']['message']['text'])
                    id = update['callback_query']['message']['message_id']
                    await self.editMessage(id, answer)
                
                elif 'message' in update:
                    handlers = handlers_message
                    param = update['message']['text']
                
                else:
                    continue

                for handler in handlers:
                    answer = handler.execute(param)
                    if answer:
                        await self.sendMessage(answer)
                        break
            else:
                logging.debug('Bot: No updates.')
        self.stop = 0

    async def sendMessage(self, answer: Answer):
        params = {
            'text': answer.text,  
            'chat_id': user_id
            }
        if answer.reply_markup:
            params['reply_markup'] = answer.reply_markup
        await self.get('sendMessage', params)

    async def editMessage(self, id, answer: Answer):
        params = {
            'text': answer.text,  
            'chat_id': user_id,
            'message_id': id
            }
        if answer.reply_markup:
            params['reply_markup'] = answer.reply_markup
        await self.get('editMessageText', params)

    def stopPolling(self):
        self.stop = 1
        logging.debug("Polling has stopped. Token: {}.".format(self.token))